//
//  Contact+CoreDataClass.swift
//  ContactsTest
//
//  Created by Rafael Perez on 6/11/17.
//  Copyright © 2017 Eleven Systems. All rights reserved.
//

import Foundation
import CoreData

@objc(Contact)
public class Contact: NSManagedObject {
    
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext!) {
        super.init(entity: entity, insertInto: context)
    }
    
    public func save(firstName: String,lastName: String,phoneNumber: String, streetAddress1: String, streetAddress2: String,city: String, state: String, zipCode: String, managedObjectContextEdit: NSManagedObjectContext?, managedObjectEdit: NSManagedObject?) {
        
        
        if managedObjectContextEdit == nil {
            self.setValue("11", forKey: "contactID")
            self.setValue(firstName, forKeyPath: "firstName")
            self.setValue(lastName, forKeyPath: "lastName")
            self.setValue(phoneNumber ,forKeyPath: "phoneNumber")
            self.setValue(streetAddress1, forKeyPath: "streetAddress1")
            self.setValue(streetAddress2, forKeyPath: "streetAddress2")
            self.setValue(state, forKeyPath: "state")
            self.setValue(zipCode, forKeyPath: "zipCode")
            self.setValue(city, forKeyPath: "city")
            do {
                try managedObjectContext!.save()
                let isSaved = !objectID.isTemporaryID;
                print(isSaved)
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
        else{
            managedObjectEdit?.setValue("11", forKey: "contactID")
            managedObjectEdit?.setValue(firstName, forKeyPath: "firstName")
            managedObjectEdit?.setValue(lastName, forKeyPath: "lastName")
            managedObjectEdit?.setValue(phoneNumber ,forKeyPath: "phoneNumber")
            managedObjectEdit?.setValue(streetAddress1, forKeyPath: "streetAddress1")
            managedObjectEdit?.setValue(streetAddress2, forKeyPath: "streetAddress2")
            managedObjectEdit?.setValue(state, forKeyPath: "state")
            managedObjectEdit?.setValue(zipCode, forKeyPath: "zipCode")
            managedObjectEdit?.setValue(city, forKeyPath: "city")
            do {
                try managedObjectContextEdit!.save()
                let isSaved = !objectID.isTemporaryID;
                print(isSaved)
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
            
        }
        
    }
    
    public func delete(manangedObjectToDelete: NSManagedObject?, managedObjectContextToDelete: NSManagedObjectContext?) {
        managedObjectContextToDelete?.delete(manangedObjectToDelete!)
        do {
            try managedObjectContextToDelete!.save()
            let isSaved = !objectID.isTemporaryID;
            print(isSaved)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        
    }
    
    
    
    
    
    
    
}
