//
//  ContactViewController.swift
//  ContactsTest
//
//  Created by Rafael Perez on 6/10/17.
//  Copyright © 2017 Eleven Systems. All rights reserved.
//

import UIKit
import CoreData


class ContactViewController: UIViewController {
    @IBOutlet weak var lblfirstName: UILabel!
    @IBOutlet weak var lbllastName: UILabel!
    @IBOutlet weak var lblphoneNumber: UILabel!
    @IBOutlet weak var lblstreetAddress1: UILabel!
    @IBOutlet weak var lblstreetAddress2: UILabel!
    @IBOutlet weak var lblcity: UILabel!
    @IBOutlet weak var lblzip: UILabel!
    @IBOutlet weak var lblstate: UILabel!
    var contact : NSManagedObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblfirstName.text =
            contact.value(forKeyPath: "firstName") as? String
        lbllastName.text =
            contact.value(forKeyPath: "lastName") as? String
        lblphoneNumber.text =
            contact.value(forKeyPath: "phoneNumber") as? String
        lblstreetAddress1.text =
            contact.value(forKeyPath: "streetAddress1") as? String
        lblstreetAddress2.text =
            contact.value(forKeyPath: "streetAddress2") as? String
        lblcity.text =
            contact.value(forKeyPath: "city") as? String
        lblstate.text =
            contact.value(forKeyPath: "state") as? String
        lblzip.text =
            contact.value(forKeyPath: "zipCode") as? String
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier=="editContactSegue"{
            let destinationVC = segue.destination as! EditContactViewController
            destinationVC.contact = self.contact
        }
        
    }
    
}
