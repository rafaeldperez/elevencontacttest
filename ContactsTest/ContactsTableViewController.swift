//
//  ContactsTableViewController.swift
//  ContactsTest
//
//  Created by Rafael Perez on 6/10/17.
//  Copyright © 2017 Eleven Systems. All rights reserved.
//

import UIKit
import CoreData

class ContactsTableViewController: UITableViewController {
    var contacts: [NSManagedObject] = []
    var contactIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //1
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Contact")
        
        //3
        do {
            self.contacts = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        self.tableView.reloadData()
        if contacts.isEmpty {
            if let path = Bundle.main.path(forResource: "Contacts", ofType: "json")
            {
                
                
                if let jsonData = NSData(contentsOfFile: path){
                    
                    if let parsedData = try? JSONSerialization.jsonObject(with: jsonData as Data) as! [String:Any]
                    {
                        if let contacts : NSArray = parsedData["Contacts"] as? NSArray
                        {
                            guard let appDelegate =
                                UIApplication.shared.delegate as? AppDelegate else {
                                    return
                            }
                            
                            let managedContext =
                                appDelegate.persistentContainer.viewContext
                            
                            let entity =
                                NSEntityDescription.entity(forEntityName: "Contact",
                                                           in: managedContext)!
                            for x in contacts {
                                let contact = NSManagedObject(entity: entity,
                                                              insertInto: managedContext)
                                
                                contact.setValue((x as! NSDictionary).object(forKey: "contactID"), forKey: "contactID")
                                contact.setValue((x as! NSDictionary).object(forKey: "firstName"), forKeyPath: "firstName")
                                contact.setValue((x as! NSDictionary).object(forKey: "lastName"), forKeyPath: "lastName")
                                contact.setValue((x as! NSDictionary).object(forKey: "phoneNumber") ,forKeyPath: "phoneNumber")
                                contact.setValue((x as! NSDictionary).object(forKey: "streetAddress1"), forKeyPath: "streetAddress1")
                                contact.setValue((x as! NSDictionary).object(forKey: "streetAddress2"), forKeyPath: "streetAddress2")
                                contact.setValue((x as! NSDictionary).object(forKey: "state"), forKeyPath: "state")
                                contact.setValue((x as! NSDictionary).object(forKey: "zipCode"), forKeyPath: "zipCode")
                                contact.setValue((x as! NSDictionary).object(forKey: "city"), forKeyPath: "city")
                                
                                do {
                                    try managedContext.save()
                                    
                                } catch let error as NSError {
                                    print("Could not save. \(error), \(error.userInfo)")
                                }
                            }
                            
                            let fetchRequest =
                                NSFetchRequest<NSManagedObject>(entityName: "Contact")
                            
                            do {
                                self.contacts = try managedContext.fetch(fetchRequest)
                            } catch let error as NSError {
                                print("Could not fetch. \(error), \(error.userInfo)")
                            }
                            self.tableView.reloadData()
                        }
                    }
                    
                }
                
            }
        }
        
        
        
        
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return contacts.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath)
        
        let contact = contacts[indexPath.row]
        
        cell.textLabel?.text =
            String(format: "%@ %@", contact.value(forKeyPath: "firstName") as! CVarArg, contact.value(forKeyPath: "lastName") as! CVarArg)
        
        
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        contactIndex = indexPath.row
        self .performSegue(withIdentifier: "contactSegue", sender: self)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="contactSegue"{
            let destinationVC = segue.destination as! ContactViewController
            destinationVC.contact = contacts[contactIndex]
        }
        
    }
    
    
}
