//
//  EditContactViewController.swift
//  ContactsTest
//
//  Created by Rafael Perez on 6/11/17.
//  Copyright © 2017 Eleven Systems. All rights reserved.
//

import UIKit
import CoreData

class EditContactViewController: UIViewController {
    var contacts: [NSManagedObject] = []
    @IBOutlet weak var txtfirstName: UITextField!
    @IBOutlet weak var txtlastName: UITextField!
    @IBOutlet weak var txtphoneNumber: UITextField!
    @IBOutlet weak var txtstreetAddress1: UITextField!
    @IBOutlet weak var txtstreetAddress2: UITextField!
    @IBOutlet weak var txtcity: UITextField!
    @IBOutlet weak var txtzip: UITextField!
    @IBOutlet weak var txtstate: UITextField!
    var contact : NSManagedObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtfirstName.text = contact.value(forKeyPath: "firstName") as! String?
        txtlastName.text = contact.value(forKeyPath: "lastName") as! String?
        txtphoneNumber.text = contact.value(forKeyPath: "phoneNumber") as! String?
        txtstreetAddress1.text = contact.value(forKeyPath: "streetAddress1") as! String?
        txtstreetAddress2.text = contact.value(forKeyPath: "streetAddress2") as! String?
        txtcity.text = contact.value(forKeyPath: "city") as! String?
        txtstate.text = contact.value(forKeyPath: "state") as! String?
        txtzip.text = contact.value(forKeyPath: "zipCode") as! String?
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(sender: AnyObject) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func done(sender: AnyObject) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        
        let entity =
            NSEntityDescription.entity(forEntityName: "Contact",
                                       in: managedContext)!
        let contact = Contact(entity: entity, insertInto: nil)
        
        
        contact.save(firstName: txtfirstName.text!, lastName: txtlastName.text!, phoneNumber: txtphoneNumber.text!, streetAddress1: txtstreetAddress1.text!, streetAddress2: txtstreetAddress2.text!, city: txtcity.text!, state: txtstate.text!, zipCode: txtzip.text!, managedObjectContextEdit:self.contact.managedObjectContext,managedObjectEdit: self.contact)
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func deleteObject(sender: AnyObject) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        
        let entity =
            NSEntityDescription.entity(forEntityName: "Contact",
                                       in: managedContext)!
        let contact = Contact(entity: entity, insertInto: nil)
        contact.delete(manangedObjectToDelete: self.contact, managedObjectContextToDelete: self.contact.managedObjectContext!)
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    
}
