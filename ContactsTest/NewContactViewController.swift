//
//  NewContactViewController.swift
//  ContactsTest
//
//  Created by Rafael Perez on 6/10/17.
//  Copyright © 2017 Eleven Systems. All rights reserved.
//

import UIKit
import CoreData

class NewContactViewController: UIViewController {
    var contacts: [NSManagedObject] = []
    @IBOutlet weak var txtfirstName: UITextField!
    @IBOutlet weak var txtlastName: UITextField!
    @IBOutlet weak var txtphoneNumber: UITextField!
    @IBOutlet weak var txtstreetAddress1: UITextField!
    @IBOutlet weak var txtstreetAddress2: UITextField!
    @IBOutlet weak var txtcity: UITextField!
    @IBOutlet weak var txtzip: UITextField!
    @IBOutlet weak var txtstate: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func done(sender: AnyObject) {
        if txtfirstName.text!.isEmpty || txtlastName.text!.isEmpty {
            let alert = UIAlertController(title: "Alert", message: "Please complete the fields to add a new contact", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
            }
            let managedContext =
                appDelegate.persistentContainer.viewContext
            
            
            let entity =
                NSEntityDescription.entity(forEntityName: "Contact",
                                           in: managedContext)!
            let contact = Contact(entity: entity, insertInto: managedContext)
            
            
            contact.save(firstName: txtfirstName.text!, lastName: txtlastName.text!, phoneNumber: txtphoneNumber.text!, streetAddress1: txtstreetAddress1.text!, streetAddress2: txtstreetAddress2.text!, city: txtcity.text!, state: txtstate.text!, zipCode: txtzip.text!,managedObjectContextEdit: nil, managedObjectEdit: nil)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    
}
